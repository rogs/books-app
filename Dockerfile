FROM python:3.9.0-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --update --no-cache postgresql-client python3-dev

RUN apk add --update --no-cache --virtual .tmp-build-deps \
  gcc libc-dev linux-headers postgresql-dev g++

RUN mkdir /app
WORKDIR /app

ADD requirements.txt /app
RUN pip install -r ./requirements.txt
RUN apk del .tmp-build-deps

ADD app/ /app

RUN adduser -D user
RUN chown -R user:user .
USER user
