"""
URLs for the order
"""
from django.urls import path, include

from rest_framework import routers

from books import views

router = routers.DefaultRouter()

router.register(r"book", views.BookViewSet, basename="book")
router.register(r"author", views.AuthorViewSet, basename="author")
router.register(r"people", views.PeopleViewSet, basename="people")
router.register(r"subject", views.SubjectViewSet, basename="subject")

app_name = "book"

urlpatterns = [
    path("", include(router.urls)),
    path("bulk-create", views.BulkCreateBook.as_view(), name="bulk-book"),
]
