"""
Books admin
"""

from django.contrib import admin

from books import models


class BaseAdmin(admin.ModelAdmin):
    """A base admin that sets the ID as readonly"""

    readonly_fields = ["id"]


admin.site.register(models.Book, BaseAdmin)
admin.site.register(models.Author, BaseAdmin)
admin.site.register(models.People, BaseAdmin)
admin.site.register(models.Subject, BaseAdmin)
